from sklearn import metrics
from sklearn.metrics import make_scorer, f1_score, precision_score, recall_score
from yellowbrick.classifier import ROCAUC

def classification_report(model, label_encoder, X_test, y_test):
  y_pred = label_encoder.inverse_transform(model.predict(X_test))
  y_test_inv = label_encoder.inverse_transform(y_test)
  print(metrics.classification_report(y_test_inv, y_pred, digits=3))

def get_scoring():
  scores = {
    'f1_macro': make_scorer(f1_score, average='macro'),
    'prec_macro': make_scorer(precision_score, average='macro'),
    'rec_macro': make_scorer(recall_score, average='macro')
  }
  return scores

def summarize_results(result):
  # Summarise results
  print(f"Best: {result.best_score_} using {result.best_params_}")
  means = result.cv_results_['mean_test_f1_macro']
  stds = result.cv_results_['std_test_f1_macro']
  params = result.cv_results_['params']

  for mean, stdev, param in zip(means, stds, params):
    print(f"{mean} F1-Macro ({stdev} STD) with: {param}")

def plot_ROC_curve(model, X_train, y_train, X_test, y_test, label_encoder):
  # Creating visualisation with the readable labels
  visualizer = ROCAUC(model, encoder={0: 'Exchange', 
                                      1: 'Gitcoin Grants', 
                                      2: 'ICO Wallets',
                                      3: 'Mining',
                                      4: 'Others (Dodgy)',
                                      5: 'Others (Legit)',
                                      6: 'Phish / Hack',
                                      7: 'Scamming',
                                      8: 'Upbit Hack'})
                                      
  # Fitting to the training data first then scoring with the test data
  visualizer.fit(X_train, y_train)
  visualizer.score(X_test, y_test)
  visualizer.show()
  
  return visualizer