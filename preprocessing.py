import pandas as pd
import seaborn as sns
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import StandardScaler
from imblearn.over_sampling import SMOTE
import matplotlib.pyplot as plt

def modify_cat(df, row):
  # Categories that have less than 50 samples are
  # categorised under Others (Legit) or Others (Dodgy) accordingly
  if (df['category']==row['category']).sum() < 50:
    return 'Others (%s)' % row['label']
  else:
    return row['category']

def load_data():
  # Extract address profiles dataset into pandas dataframe
  df = pd.read_csv("./Data/address_profiles.csv")
  # Drop rows with null values
  df = df.dropna()
  # Merge categories with few samples
  df['category'] = df.apply(lambda row: modify_cat(df, row), axis=1)
  return df

def explore_data():
  df = load_data()
  # Number of Samples per category
  display(
      df[['category', 'address']]
        .groupby(['category'])
        .count()
        .sort_values(['address'], ascending=False)
  )
  df['category'].value_counts().plot.bar()
  plt.show()

  df = preprocess_data(df)
  print(df.info())

def preprocess_data(df):
  # Drop irrelavant columns
  cols = ['address', 'label']
  df = df.drop(cols, axis=1)

  # Typecasting to float
  cols = ['eth_balance', 'total_eth_sent', 
                   'min_eth_sent', 'max_eth_sent', 
                   'total_eth_recv', 'min_eth_recv', 
                   'max_eth_recv']
  df[cols] = df[cols].astype('float')

  # Typecasting to integer
  cols = ['txns_sent', 'txns_recv']
  df[cols] = df[cols].astype('int')

  # Convert from Wei to ETH (1 ETH = 10^18 Wei)
  cols = ['eth_balance', 'total_eth_sent', 
          'min_eth_sent', 'max_eth_sent', 
          'avg_eth_sent', 'total_eth_recv', 
          'min_eth_recv', 'max_eth_recv', 'avg_eth_recv']
  df[cols] = df[cols].div(1000000000000000000)

  # Round minute value columns to 2 decimal places
  cols = ['time_diff_btween_first_and_last_in_mins', 
          'avg_min_between_txn_sent', 
          'avg_min_between_txn_recv']
  df[cols] = df[cols].round(2)

  return df

def xy_split(df):
  # Separate the classes from the features
  X = df.iloc[:,1:]
  y = df.iloc[:,0]
  return X, y

def encode_labels(X, y):
  # Encode target labels (y) with values between 0 and n_classes-1
  label_encoder = LabelEncoder()
  label_encoder.fit(y)
  y = label_encoder.transform(y)

  return y, label_encoder

def smote(X_train, y_train):
  sm = SMOTE(random_state=7)
  X_res, y_res = sm.fit_resample(X_train, y_train)
  return X_res, y_res